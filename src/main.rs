use std::env;
use futures_util::StreamExt;
use rutebot::client::Rutebot;
use rutebot::requests::{SendMessage, ParseMode};
use rutebot::responses::Message;
use std::error::Error;
use rand::random;
use std::collections::HashMap;
use std::process::Command;

fn is_from_bot(msg: &Message, bots: &Vec<&str>) -> bool {
    if let Some(user) = msg.from.clone() {
        if let Some(username) = user.username {
            if bots.iter().any(|b| username == *b) {
                return true;
            } 
        }
    }
    return false;
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    let token_env = env::var_os("TELEGRAM_TOKEN")
        .expect("Please specify your bot's token in the TELEGRAM_TOKEN environment variable.");
    let token = token_env.to_string_lossy();

    let bots: Vec<&str> = vec![
        "Ventu06",
        "Pit_w",
    ];

    let triggers = vec![
        "mastruzzare",
        "storzellare",
        "dampare",
        "scatterare",
        "poppy",
        "vandervals",
        "ellissoido",
        "epsilonijk",
        "bilanciere",

    ];

    let replies = vec![
        "questo lo facevano i miei figli all'asilo",
        "beh, si vede",
        "pota si vede vecio",
        "questo è tabulato",
        "lo vedrete giovedì con Maicol",
        "e fu giorno e fu mattino, prima rotazione terrestre",
        "i tensori sono cose che ruotano come i tensori",
        "i tensori sono cose che ruotano come i vettori",
        "i vettori sono indipendenti dal sistema di riferimento",
        "scriviamolo in sferiche",
        "it's not like finding a poppy in a cornfield, it's like finding a mice in a cornfield",
        "😎",
    ];

    let mut msg_counter: HashMap::<i64, (u8, u8)> = HashMap::new();

    let rutebot = Rutebot::new(token);
    let mut updates_stream = Box::pin(rutebot.incoming_updates(None, None));
    while let Some(update) = updates_stream.next().await.transpose()? {
        if let Some(message) = update.message {
            let msg_text = message.text.clone();
            if let Some(text) = msg_text {
                let mut text = text.clone();
                text.make_ascii_lowercase();
                let cnt = msg_counter.get(&message.chat.id);
                if text.contains("hey") {
                    let reply = SendMessage::new_reply(
                        message.chat.id,
                        if is_from_bot(&message, &bots) {
                            "Bots cannot read other Bot's messages"
                        } else {
                            "Hey!"
                        },
                        message.message_id
                    );

                    tokio::spawn(rutebot.prepare_api_request(reply).send());
                } else if triggers.iter().any(|t| text.contains(t)) {
                    let random_reply: &str = replies[random::<usize>() % replies.len()];
                    let reply = SendMessage::new_reply(
                        message.chat.id,
                        if is_from_bot(&message, &bots) {
                            "Bots cannot read other Bot's messages"
                        } else {
                            random_reply
                        },
                        message.message_id
                    );

                    tokio::spawn(rutebot.prepare_api_request(reply).send());
                } else if let Some((c, t)) = cnt {
                    if c >= t {
                        let output = String::from_utf8(
                            Command::new("./fc.sh").output()?.stdout
                        )?;
                        let mut reply = SendMessage::new(
                            message.chat.id,
                            &output,
                        );

                        reply.parse_mode = Some(ParseMode::Markdown);

                        tokio::spawn(rutebot.prepare_api_request(reply).send());
                        let t: u8 = 15 + (random::<u8>() % 6) as u8;
                        //let t: u8 = 1;
                        msg_counter.insert(message.chat.id, (0, t));
                    } else {
                        let c = *c;
                        let t = *t;
                        msg_counter.insert(message.chat.id, (c+1, t));
                    }
                } else {
                    let t: u8 = 15 + (random::<u8>() % 6) as u8;
                    //let t: u8 = 1;
                    msg_counter.insert(message.chat.id, (0, t));
                }
            }
        }
    }
    Ok(())
}

